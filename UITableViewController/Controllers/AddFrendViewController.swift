//
//  AddFrendViewController.swift
//  UITableViewController
//
//  Created by Матвей Захаров on 31.03.2023.
//

import UIKit

class AddFrendViewController: UIViewController, UITextFieldDelegate {


    @IBOutlet private weak var phoneLabel: UILabel!
    @IBOutlet private weak var sexLabel: UILabel!
    @IBOutlet private weak var ageLabel: UILabel!
    @IBOutlet private weak var nameLable: UILabel!
    
    @IBOutlet private weak var adresTextField: UITextField!
    @IBOutlet private weak var secondNameTextFeild: UITextField!
    @IBOutlet private weak var phoneTextField: UITextField!
    @IBOutlet private weak var emailTextFeild: UITextField!
    @IBOutlet private weak var sexTextFeild: UITextField!
    @IBOutlet private weak var ageTextFeild: UITextField!
    @IBOutlet private weak var nameTextFeild: UITextField!
    
    @IBOutlet private weak var button: UIButton!
    
    private var newfrend = Model()
    private let maxNumberCount = 11
    private let regex = try! NSRegularExpression(pattern: "[\\+\\s-\\(\\)]", options: .caseInsensitive)
   

    
    override func viewDidLoad() {
        super.viewDidLoad()

        button.layer.cornerRadius = 15
        secondNameTextFeild.delegate = self
        nameTextFeild.delegate = self
        adresTextField.delegate = self
        emailTextFeild.delegate = self
        phoneTextField.delegate = self
    
    }
    
    @IBAction func buttonDidPressed(_ sender: UIButton) {
        defaultTextField(label: nameLable, textField: nameTextFeild)
        defaultTextField(label: ageLabel, textField: ageTextFeild)
        defaultTextField(label: sexLabel, textField: sexTextFeild)
        defaultTextField(label: phoneLabel, textField: phoneTextField)
        
        if nameTextFeild.text != "" && ageTextFeild.text != "" && sexTextFeild.text != "" && phoneTextField.text != ""{
            newfrend = Model(firstName: nameTextFeild.text!, secondName: secondNameTextFeild.text ?? "", age: ageTextFeild.text!, sex: sexTextFeild.text!, phoneNumber: phoneTextField.text, workAdress: adresTextField.text ?? "", email: emailTextFeild.text ?? "")
            frendsArray.append(newfrend)
            navigationController?.popViewController(animated: true)
        }
    }
    
    private func defaultTextField(label: UILabel, textField: UITextField){
        if checkTextField(textField: textField, label: label){
            textField.layer.borderWidth = 0.0
            label.textColor = UIColor.darkGray
        }
    }
    private func checkTextField(textField: UITextField, label: UILabel) -> Bool{
       if textField.text == "" {
           textField.layer.masksToBounds = true
           textField.layer.borderColor = UIColor.systemRed.cgColor
           textField.placeholder = "Обязательное поле для заполнения"
           textField.layer.borderWidth = 1.0
           textField.layer.cornerRadius = 10
           label.textColor = UIColor.systemRed
           return false
        }
        return true
    }
    
// MARK: UITextFieldDelegate
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == phoneTextField || textField == emailTextFeild || textField == adresTextField {
            NotificationCenter.default.addObserver(self,
                                                   selector: #selector(keyboardWillShow),
                                                   name: UIResponder.keyboardWillShowNotification,
                                                   object: nil)
            NotificationCenter.default.addObserver(self,
                                                   selector: #selector(keyboardWillHide),
                                                   name: UIResponder.keyboardWillHideNotification,
                                                   object: nil)
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    @objc func keyboardWillShow (notification: NSNotification){
        guard let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else {return}
        if self.view.frame.origin.y == 0 {
            self.view.frame.origin.y -= keyboardSize.height
        }
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
    }
    
    @objc func keyboardWillHide (notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    private func format(phoneNumber: String, shouldRemoveLastDigit: Bool) -> String {
        guard !(shouldRemoveLastDigit && phoneNumber.count <= 2) else{return ""}
        
        let range = NSString(string: phoneNumber).range(of: phoneNumber)
        var number  = regex.stringByReplacingMatches(in: phoneNumber, options: [], range: range, withTemplate: "")
        
        if number.count > maxNumberCount {
            let maxIndex = number.index(number.startIndex, offsetBy: maxNumberCount)
            number = String(number[number.startIndex..<maxIndex])
        }
        
        if shouldRemoveLastDigit {
            let maxIndex = number.index(number.startIndex, offsetBy: number.count - 1)
            number = String(number[number.startIndex..<maxIndex])
        }
        
        let maxIndex = number.index(number.startIndex , offsetBy: number.count)
        let regRange = number.startIndex..<maxIndex
        
        if number.count < 7 {
            let pattern = "(\\d)(\\d{3})(\\d+)"
            number = number.replacingOccurrences(of: pattern, with: "$1 ($2) $3", options: .regularExpression, range: regRange)
        }else {
            let pattern = "(\\d)(\\d{3})(\\d{3})(\\d{2})(\\d+)"
            number = number.replacingOccurrences(of: pattern, with: "$1 ($2) $3-$4-$5", options: .regularExpression, range: regRange)
        }
        return "+" + number
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == phoneTextField{
            let fullString  = (textField.text ?? "") + string
            textField.text = format(phoneNumber: fullString, shouldRemoveLastDigit: range.length == 1)
            return false
        }
        return true
    }
}
 
