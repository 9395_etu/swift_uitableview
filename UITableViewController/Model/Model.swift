//
//  Model.swift
//  UITableViewController
//
//  Created by Матвей Захаров on 31.03.2023.
//

import Foundation

struct Model {
    var firstName: String?
    var secondName: String?
    var age: String?
    var sex: String?
    var phoneNumber: String?
    var workAdress: String?
    var email: String?
}
